/**
 * Shared Directives Module
 */

import angular from 'angular';
import ngUtilsPagination from 'angular-utils-pagination';

// import shared directives
import { MainNavDirective } from './main-navigation';
import { GridViewDirective } from './grid-view';
import { SpinnerDirective } from './spinner';

// define directives module
const SharedDirectivesModule = angular.module('app.directives', [
	'angularUtils.directives.dirPagination'
]);

// define all directives
SharedDirectivesModule.directive('mainNavigation', MainNavDirective.factory);
SharedDirectivesModule.directive('gridView', GridViewDirective.factory);
SharedDirectivesModule.directive('spinner', SpinnerDirective.factory);


export { SharedDirectivesModule };
