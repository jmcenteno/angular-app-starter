/**
 * Main Navigation Directive
 */
class MainNavDirective {

	constructor () {

		this.restrict = 'EA';
		this.templateUrl = './app/shared/directives/main-navigation/template.html';
		this.replace = true;
		this.scope = {};

	}

	link (scope, element, attrs) {

	}

	static factory () {

		MainNavDirective.instance = new MainNavDirective();
		return MainNavDirective.instance;

	}

}

export { MainNavDirective };
