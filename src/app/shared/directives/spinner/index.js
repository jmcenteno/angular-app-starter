/**
 * Spinner
 * @description Render the markup to display a spinner animation
 */
class SpinnerDirective {
  
  constructor () {
  	
  	this.restrict = 'E';
  	this.scope = {};
  	this.replace = true;
  	this.template = '' +
  		'<div class="spinner">' + 
      '<div class="bounce1"></div>' +
      '<div class="bounce2"></div>' + 
      '<div class="bounce3"></div>' + 
      '</div>';
      
  }

  link (scope, element, attrs) {

  }

  static factory () {

    SpinnerDirective.instance = new SpinnerDirective();
    return SpinnerDirective.instance;

  }

}

export { SpinnerDirective };
