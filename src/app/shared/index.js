import { SharedControllersModule } from './controllers';
import { SharedDirectivesModule } from './directives';
import { SharedServicesModule } from './services';
import { SharedFiltersModule } from './filters'

export default {
	SharedControllersModule,
	SharedDirectivesModule,
	SharedServicesModule,
	SharedFiltersModule
}
