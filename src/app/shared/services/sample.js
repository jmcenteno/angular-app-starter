/**
 * SampleSharedService
 * @description A pretty simple sample service
 */
class SampleSharedService {

	constructor ($http, $q, API_ENDPOINT) {
		'ngInject';

		// make services available in other methods
		this.endpoint = API_ENDPOINT;
		this._$http = $http;
		this._$q = $q;

	}

	getSampleData () {

		let deferred = this._$q.defer();

		this._$http.get(this.endpoint + '/planets').success(function (data) {
			deferred.resolve(data);
		}).error(function (error) {
			deferred.reject(error);
		});

		return deferred.promise;

	}

	static factory ($http, $q, API_ENDPOINT) {
		'ngInject';

		return new SampleSharedService ($http, $q, API_ENDPOINT);

	}

}

export { SampleSharedService };
