/**
 * Shared Services Module
 */

import angular from 'angular';

// import shared services
import { SampleSharedService } from './sample';

// define services module
const SharedServicesModule = angular.module('app.services', []);

// define all shared services
SharedServicesModule.factory('SampleSharedFactory', SampleSharedService.factory);

export { SharedServicesModule };
