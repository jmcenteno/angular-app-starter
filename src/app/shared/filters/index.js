/**
 * Shared Filters Module
 */

import angular from 'angular';

// import shared custom filters
import { SampleSharedFilter } from './sample.js'

// define filters module
const SharedFiltersModule = angular.module('app.filters', []);

// define all shared filters
SharedFiltersModule.filter('sampleSharedFilter', SampleSharedFilter);

export { SharedFiltersModule };
