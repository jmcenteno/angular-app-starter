/**
 * Shared Controllers Module
 */

import angular from 'angular';

// import shared controller functions
import MainController from './main';

// define controllers module
const SharedControllersModule = angular.module('app.controllers', []);

// define all shared controllers
SharedControllersModule.controller('MainCtrl', MainController);

export { SharedControllersModule };
