/**
 * Main Controller
 */
function MainCtrl ($window, $rootScope, APP_NAME, COMPANY_NAME) {
	'ngInject';

	const $ctrl = this;

	$rootScope.APP_NAME = APP_NAME;
	
	$ctrl.isCollapsed = ($window.innerWidth > 768);
	$ctrl.COMPANY_NAME = COMPANY_NAME;	
	$ctrl.year = (new Date()).getFullYear();

}

export default MainCtrl;
