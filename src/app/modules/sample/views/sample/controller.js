/**
 * Sample Controller
 */
function SampleController ($scope, $rootScope, SampleData) {
	'ngInject';

	const $ctrl = this;

	$rootScope.pageTitle = 'Sample Route'

	// grid-view configuration
  $ctrl.gridViewConfig = {

  	// table headers
    headers: [
      {
        label: 'Name',
        field: 'name',
        sortable: true
      },
      {
        label: 'Rotation Period',
        field: 'rotation_period',
        sortable: true
      },
      {
        label: 'Orbital Period',
        field: 'orbital_period',
        sortable: true
      },
      {
        label: 'Diameter',
        field: 'diameter',
        sortable: true
      },
      {
        label: 'Climate',
        field: 'climate',
        sortable: true
      },
      {
        label: 'Population',
        field: 'population',
        sortable: true
      }
    ],

    data: SampleData,

    // initial sorting options
    sortType: 'firstName', 
    sortReverse: false,
    scrollable: true,
  };

}

export { SampleController };
