/**
 * Dashboard Module
 */
import angular from 'angular';

// import all controllers
import { SampleController } from './views/sample/controller';

// define the module
const module = angular.module('app.sample', []);

// define module controllers
module.controller('SampleCtrl', SampleController);


export default module;
