/**
 * Sample Controller
 */
function HomeController ($scope, $rootScope) {
	'ngInject';

	const $ctrl = this;

	$rootScope.pageTitle = 'Home';

}

export { HomeController };
