/**
 * Dashboard Module
 */
import angular from 'angular';

// import all controllers
import { HomeController } from './views/home/controller';

// define the module
const module = angular.module('app.home', []);

// define module controllers
module.controller('HomeCtrl', HomeController);


export default module;
