// import all feature modules
import HomeModule from './home';
import SampleModule from './sample';

export default {
	HomeModule,
	SampleModule
};
