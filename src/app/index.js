// load 3rd party modules
import vendors from './vendors';

// load app cofiguration
import Config from './config';

// load app modules
import SharedModules from './shared';
import FeatureModules from './modules';

// load bootstraping function
import RunApplication from './run';

// main module definition
const app = angular.module('app', [
  
  // third party modules
  'ui.router',
  'ngAnimate',
  'ngSanitize',
  'ngAria',

  // shared modules
  'app.controllers',
  'app.directives',
  'app.services',
  'app.filters',

  // modules by feature
  'app.home',
  'app.sample'

]);

// define app conttants
for (let key in Config.Constants) {
  app.constant(key, Config.Constants[key]);  
}

// define app configuration
app.config(Config.RoutesConfig)
app.config(Config.InterceptorsConfig)

// bootstrap the appilcation
app.run(RunApplication);


export default app;
