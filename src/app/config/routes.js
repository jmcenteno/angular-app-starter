/**
 * Application Routes
 */
function RoutesConfig ($stateProvider, $urlRouterProvider) {
  'ngInject';

  $stateProvider

    // base route
    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'app/shared/templates/layouts/main.html',
      controller: 'MainCtrl as $ctrl'
    })

    // home route
    .state('app.home', {
      url: '/home',
      views: {
        main: {
          templateUrl: 'app/modules/home/views/home/template.html',
          controller: 'HomeCtrl as $ctrl'
        }
      }
    })

    // sample route
    .state('app.sample', {
      url: '/sample',
      views: {
        main: {
          templateUrl: 'app/modules/sample/views/sample/template.html',
          controller: 'SampleCtrl as $ctrl'
        }
      },
      resolve: {
        SampleData: function (SampleSharedFactory) {
          return SampleSharedFactory.getSampleData().then(function (data) {
            return data.results;
          }, function () {
            return [];
          })
        }
      }
    });

    // redirect to customers view as a failsafe
    $urlRouterProvider.otherwise('/app/home');

};

export { RoutesConfig };
