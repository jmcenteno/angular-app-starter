/**
 * Define all application constants here
 */

export const APP_NAME = 'Sample App';
export const COMPANY_NAME = 'Company Name';
export const API_ENDPOINT = 'http://swapi.co';
