function InterceptorsConfig ($httpProvider) {
  'ngInject';
  
  const requestsInterceptor = function($q, $injector) {
    'ngInject';

    return {

      request: function(config) {      
        return config;
      },

      response: function(response) {
        return response;
      },

      responseError: function(rejection) {

        let msg = 'We\'re unable to process your request.';

        switch (rejection.status) {

          // handle 404 errors
          case 404 :

            break;

          // handle 500 error
          case 500 : 

            break;

          // do whatever you want here  
          default : 
            console.log(rejection.status);
            break;

        }

        return $q.reject(rejection);

      }

    }

  }

  $httpProvider.interceptors.push(requestsInterceptor);

}

export { InterceptorsConfig };
